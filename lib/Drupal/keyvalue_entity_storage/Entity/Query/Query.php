<?php

/**
 * @file
 * Contains \Drupal\keyvalue_entity_storage\Entity\Query\Query.
 */

namespace Drupal\keyvalue_entity_storage\Entity\Query;

use Drupal\Core\Config\Entity\Query\Query as QueryParent;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Defines the entity query for entities stored in a key value backend.
 */
class Query extends QueryParent {

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * @todo Except the KV factory, this is method is the same as
   *   \Drupal\Core\Entity\Query\QueryBase. The loadRecords() method is the only
   *   other part needing to be overridden, otherwise this is identical to
   *   \Drupal\Core\Config\Entity\Query\Query.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param string $conjunction
   *   - AND: all of the conditions on the query need to match.
   *   - OR: at least one of the conditions on the query need to match.
   * @param array $namespaces
   *   List of potential namespaces of the classes belonging to this query.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   */
  public function __construct(EntityTypeInterface $entity_type, $conjunction, array $namespaces, KeyValueFactoryInterface $key_value_factory) {
    $this->entityTypeId = $entity_type->id();
    $this->entityType = $entity_type;
    $this->conjunction = $conjunction;
    $this->namespaces = $namespaces;
    $this->condition = $this->conditionGroupFactory($conjunction);
    if ($this instanceof QueryAggregateInterface) {
      $this->conditionAggregate = $this->conditionAggregateGroupFactory($conjunction);
    }
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function loadRecords() {
    return $this->keyValueFactory->get('keyvalue_entity_storage__' . $this->entityTypeId)->getAll();
  }

  /**
   * {@inheritdoc}
   */
  public static function getClass(array $namespaces, $short_class_name) {
    // Use the config entity implementation.
    // @todo Stop doing this.
    $namespaces = array('\Drupal\Core\Config\Entity\Query');
    return parent::getClass($namespaces, $short_class_name);
  }

}
