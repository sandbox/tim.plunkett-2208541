<?php

/**
 * @file
 * Contains \Drupal\keyvalue_entity_storage\Entity\KeyValueEntityStorage.
 */

namespace Drupal\keyvalue_entity_storage\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageControllerBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a key value backend for entities.
 */
class KeyValueEntityStorage extends EntityStorageControllerBase {

  /**
   * The key value store.
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Constructs a new KeyValueEntityStorage.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $key_value
   *   The key value store.
   */
  public function __construct(EntityTypeInterface $entity_type, KeyValueStoreInterface $key_value) {
    parent::__construct($entity_type);
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('keyvalue')->get('keyvalue_entity_storage__' . $entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = array()) {
    $entity_class = $this->entityType->getClass();
    $entity_class::preCreate($this, $values);

    $entity = new $entity_class($values, $this->entityTypeId);
    $entity->enforceIsNew();
    $entity->postCreate($this);

    // Modules might need to add or change the data initially held by the new
    // entity object, for instance to fill-in default values.
    $this->invokeHook('create', $entity);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    if (empty($ids)) {
      $results = $this->keyValue->getAll();
    }
    else {
      $results = $this->keyValue->getMultiple($ids);
    }
    $class = $this->entityType->getClass();
    $entities = array();
    foreach ($results as $result) {
      $entity = new $class($result, $this->entityTypeId);
      $entities[$entity->id()] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function load($id) {
    $entities = $this->loadMultiple(array($id));
    return isset($entities[$id]) ? $entities[$id] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadRevision($revision_id) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision($revision_id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    if (!$entities) {
      // If no IDs or invalid IDs were passed, do nothing.
      return;
    }

    $entity_class = $this->entityType->getClass();
    $entity_class::preDelete($this, $entities);
    foreach ($entities as $entity) {
      $this->invokeHook('predelete', $entity);
    }

    $entity_ids = array();
    foreach ($entities as $entity) {
      $entity_ids[] = $entity->id();
    }
    $this->keyValue->deleteMultiple($entity_ids);

    $entity_class::postDelete($this, $entities);
    foreach ($entities as $entity) {
      $this->invokeHook('delete', $entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    $id = $entity->id();
    if ($id === NULL || $id === '') {
      throw new EntityMalformedException('The entity does not have an ID.');
    }
    $entity->preSave($this);
    $this->invokeHook('presave', $entity);

    $hook = !$entity->isNew() ? 'update' : 'insert';

    if ($entity instanceof ConfigEntityInterface) {
      $data = $entity->getExportProperties();
    }
    else {
      // @todo Is there a better way to serialize a non-config entity?
      $data = $entity;
    }
    $this->keyValue->set($entity->id(), $data);
    $entity->enforceIsNew(FALSE);
    $entity->postSave($this, FALSE);
    $this->invokeHook($hook, $entity);

    return $hook == 'update' ? SAVED_UPDATED : SAVED_NEW;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryServicename() {
    return 'entity.query.keyvalue';
  }

}
