<?php

/**
 * @file
 * Contains \Drupal\keyvalue_entity_storage\Tests\KeyValueEntityStorageTest.
 */

namespace Drupal\keyvalue_entity_storage\Tests {

use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\keyvalue_entity_storage\Entity\KeyValueEntityStorage;

/**
 * @coversDefaultClass \Drupal\keyvalue_entity_storage\Entity\KeyValueEntityStorage
 *
 * @group Drupal
 * @group KeyValueEntityStorage
 */
class KeyValueEntityStorageTest extends UnitTestCase {

  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $entityType;
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $keyValueStore;
  /**
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\keyvalue_entity_storage\Entity\KeyValueEntityStorage
   */
  protected $entityStorage;

  public static function getInfo() {
    return array(
      'name' => 'KeyValueEntityStorage',
      'description' => '',
      'group' => 'KeyValue Entity Storage',
    );
  }

  protected function setUp() {
    parent::setUp();

    $this->entityType = $this->getMock('Drupal\Core\Entity\EntityTypeInterface');
    $entity = $this->getMockForAbstractClass('Drupal\Core\Entity\Entity', array(), '', FALSE, TRUE, TRUE, array('onSaveOrDelete'));
    $entity->expects($this->any())
      ->method('onSaveOrDelete');
    $this->entityType->expects($this->any())
      ->method('getClass')
      ->will($this->returnValue(get_class($entity)));
    $this->entityType->expects($this->any())
      ->method('id')
      ->will($this->returnValue('test_entity_type'));
    $this->keyValueStore = $this->getMock('Drupal\Core\KeyValueStore\KeyValueStoreInterface');
    $this->moduleHandler = $this->getMock('Drupal\Core\Extension\ModuleHandlerInterface');
    $this->entityStorage = new KeyValueEntityStorage($this->entityType, $this->keyValueStore);
    $this->entityStorage->setModuleHandler($this->moduleHandler);
  }

  /**
   * @covers ::create()
   */
  public function testCreate() {
    $this->moduleHandler->expects($this->at(0))
      ->method('invokeAll')
      ->with('test_entity_type_create');
    $this->moduleHandler->expects($this->at(1))
      ->method('invokeAll')
      ->with('entity_create');

    $entity = $this->entityStorage->create(array('id' => 'foo'));
    $this->assertInstanceOf('Drupal\Core\Entity\EntityInterface', $entity);
    $this->assertSame('foo', $entity->id());
    return $entity;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *
   * @covers ::save()
   *
   * @depends testCreate
   */
  public function testSaveInsert(EntityInterface $entity) {
    $this->moduleHandler->expects($this->at(0))
      ->method('invokeAll')
      ->with('test_entity_type_presave');
    $this->moduleHandler->expects($this->at(1))
      ->method('invokeAll')
      ->with('entity_presave');
    $this->moduleHandler->expects($this->at(2))
      ->method('invokeAll')
      ->with('test_entity_type_insert');
    $this->moduleHandler->expects($this->at(3))
      ->method('invokeAll')
      ->with('entity_insert');
    $this->keyValueStore->expects($this->once())
      ->method('set')
      ->with('foo', $entity);
    $return = $this->entityStorage->save($entity);
    $this->assertSame(SAVED_NEW, $return);
    return $entity;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *
   * @depends testSaveInsert
   */
  public function testSaveUpdate(EntityInterface $entity) {
    $this->moduleHandler->expects($this->at(0))
      ->method('invokeAll')
      ->with('test_entity_type_presave');
    $this->moduleHandler->expects($this->at(1))
      ->method('invokeAll')
      ->with('entity_presave');
    $this->moduleHandler->expects($this->at(2))
      ->method('invokeAll')
      ->with('test_entity_type_update');
    $this->moduleHandler->expects($this->at(3))
      ->method('invokeAll')
      ->with('entity_update');
    $this->keyValueStore->expects($this->once())
      ->method('set')
      ->with('foo', $entity);
    $return = $this->entityStorage->save($entity);
    $this->assertSame(SAVED_UPDATED, $return);
    return $entity;
  }

  /**
   * @covers ::save()
   * @expectedException \Drupal\Core\Entity\EntityMalformedException
   * @expectedExceptionMessage The entity does not have an ID.
   */
  public function testSaveInvalid() {
    $entity = $this->entityStorage->create(array());
    $this->entityStorage->save($entity);
  }

  /**
   * @covers ::load()
   */
  public function testLoad() {
    $this->keyValueStore->expects($this->once())
      ->method('getMultiple')
      ->with(array('foo'))
      ->will($this->returnValue(array(array('id' => 'foo'))));
    $entity = $this->entityStorage->load('foo');
    $this->assertInstanceOf('Drupal\Core\Entity\EntityInterface', $entity);
    $this->assertSame('foo', $entity->id());
  }

  /**
   * @covers ::loadMultiple()
   */
  public function testLoadMultipleAll() {
    $expected[] = $this->getMockEntity();
    $expected[] = $this->getMockEntity();
    $this->keyValueStore->expects($this->once())
      ->method('getAll')
      ->will($this->returnValue(array(array('id' => 'foo'), array('id' => 'bar'))));
    $entities = $this->entityStorage->loadMultiple();
    foreach ($entities as $id => $entity) {
      $this->assertInstanceOf('Drupal\Core\Entity\EntityInterface', $entity);
      $this->assertSame($id, $entity->id());
    }
  }

  /**
   * @covers ::loadMultiple()
   */
  public function testLoadMultipleIds() {
    $expected[] = $this->getMockEntity();
    $this->keyValueStore->expects($this->once())
      ->method('getMultiple')
      ->with(array('foo'))
      ->will($this->returnValue(array(array('id' => 'foo'))));
    $entities = $this->entityStorage->loadMultiple(array('foo'));
    foreach ($entities as $id => $entity) {
      $this->assertInstanceOf('Drupal\Core\Entity\EntityInterface', $entity);
      $this->assertSame($id, $entity->id());
    }
  }

  /**
   * @covers ::loadRevision()
   */
  public function testLoadRevision() {
    $this->assertSame(FALSE, $this->entityStorage->loadRevision(1));
  }

  /**
   * @covers ::deleteRevision()
   */
  public function testDeleteRevision() {
    $this->assertSame(NULL, $this->entityStorage->deleteRevision(1));
  }

  /**
   * @covers ::delete()
   */
  public function testDelete() {
    $entities = array();
    foreach (array('foo', 'bar') as $id) {
      $entity = $this->getMockEntity(array('id'));
      $entity->expects($this->once())
        ->method('id')
        ->will($this->returnValue($id));
      $entities[] = $entity;
    }
    $this->moduleHandler->expects($this->at(0))
      ->method('invokeAll')
      ->with('test_entity_type_predelete');
    $this->moduleHandler->expects($this->at(1))
      ->method('invokeAll')
      ->with('entity_predelete');
    $this->moduleHandler->expects($this->at(2))
      ->method('invokeAll')
      ->with('test_entity_type_predelete');
    $this->moduleHandler->expects($this->at(3))
      ->method('invokeAll')
      ->with('entity_predelete');
    $this->moduleHandler->expects($this->at(4))
      ->method('invokeAll')
      ->with('test_entity_type_delete');
    $this->moduleHandler->expects($this->at(5))
      ->method('invokeAll')
      ->with('entity_delete');
    $this->moduleHandler->expects($this->at(6))
      ->method('invokeAll')
      ->with('test_entity_type_delete');
    $this->moduleHandler->expects($this->at(7))
      ->method('invokeAll')
      ->with('entity_delete');

    $this->keyValueStore->expects($this->once())
      ->method('deleteMultiple')
      ->with(array('foo', 'bar'));
    $this->entityStorage->delete($entities);
  }

  /**
   * @covers ::delete()
   */
  public function testDeleteNothing() {
    $this->moduleHandler->expects($this->never())
      ->method($this->anything());
    $this->keyValueStore->expects($this->never())
      ->method('deleteMultiple');

    $this->entityStorage->delete(array());
  }

  protected function getMockEntity($methods = array()) {
    $methods[] = 'onSaveOrDelete';
    $entity = $this->getMockForAbstractClass('Drupal\Core\Entity\Entity', array(), '', FALSE, TRUE, TRUE, $methods);
    $entity->expects($this->any())
      ->method('onSaveOrDelete');
    return $entity;
  }

}

}
namespace {
  if (!defined('SAVED_NEW')) {
    define('SAVED_NEW', 1);
  }
  if (!defined('SAVED_UPDATED')) {
    define('SAVED_UPDATED', 2);
  }
}
